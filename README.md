# JERB: A package manager for scientific models and data

`jerb` is a tool that helps researchers:

1. Compare competing scientific models, and manage the results
2. Publish snapshots of their model or data sets
3. Share models with other researchers, and then update them later, without accidentally breaking anybody's code.


# Table of Contents

1. [Introduction](#introduction)
    - [What is a `jerb`?](#what-is-a-jerb)
    - [Why should I use `jerbs`?](#why-should-i-use-jerbs)
2. [Installation](#installation)
3. [Quick Tutorial](#quick-tutorial)
4. [Frequently Asked Questions](#frequently-asked-questions)


# Introduction
## What is a jerb?

A JERB (JSON Executable Resource Blob; a.k.a. jerb) is an immutable object that stores code or data in an easily-interchanged (JSON) format. We'll describe that more in the next section, but just to have a vague idea of what it looks like, consider this "Hello, world!" jerb:

```
{"jid": "a69977828a15336e7492c50a02134c1b723d648f", 
 "meta": {"date": "2017-11-17T01:18:06",
          "description": "Hello world jerb",
          "parents": [],
          "ref": "hello",
          "tags": ["example", "hello"],
          "user": "ivar"}, 
"pack": "UEFDSwAAAAIAAAAEnxN4nJ2OsU7EMBBE+3zFyjU5eQ8n50QIXXn01yGKjb0mlpI4sh1AQvw7MfkDutmZN6vJkRlcp5SVzhJbrdquMW1HcnBITjtr1Fk1Fgd2jxVteQwRXj4own1XKSzwlA/hr2FM24nt9gzYoNQalW6hllrKyoR59jnzf7rVt7CcTPRr9mERPYgbT1OAzxAnCzGEDMf7k3gAMURazFiosVDFWinyktPuvb7t55Y4ltzvS0qc6f0vE/xF8zpx8Y5uoS1lLvRZ4qVGrPFyl9ij7mUrfqpfQaJhgeYFgFZ4nNvPdJ9xgp5+QWJRal6JgompgbGphXmipWmaYaKpQZplIpBhYpCYam5uYpJobmFqnGhqlJqYMlHXgMnYbGK8lZixmZmCroGFgQEXl2NKSmqKQm5+USoXAEIEF9uwA3ic80jNycnXUSjPL8pJUVTwVMhILEtVSExJSU1RyM0vSlUoyVcoycgsViguKcrMS9fjAgCQ3xBmpQJ4nDM0MDAzMVHISM3JydcrqShhYO2KNd33/hzzluB1tu6OWz9naeWLAgDqBw3LQF1QH4W97WTVHPZuwKc2e4/mvO8=\n"}
```

Don't worry -- you won't edit these files by hand! This is just to illustrate the simple internal structure: a "jerb ID" `jid`, some metadata `meta` that defines fields that you can use to find the jerb later, and a `pack` containing some base-64 encoded binary data. You may store your jerbs in flat files, a database or in the cloud. Each jerb can contain source code, experimental data, a prediction from a model, or anything else you want.


## Why should I use jerbs?

A lot of scientific modeling is exploratory -- when we don't know what the right answer is, we must test several different hypotheses and then compare them to know which is best. However, it is often difficult to compare models that were run at different times or on different data sets. This is where jerbs can help; they are like a package manager or git repository to save exploratory scientific work. 

Jerbs help data scientists manage and share the code and data they use when modeling. They are named "jerbs" as a pun on the word "jobs", because they usually save snapshot of some small amount of work that you just did. Typically this work is something like recording a data set, fitting a model to some data, or generating a plot.

You might use jerbs as a sort of collaborative git repository:

1. To save a snapshot of the raw data you just collected from a neural recording
2. To save your data after converting it into CSV format, so that your colleagues can download a copy of the data after you email them the jerb's URL
3. To develop code just like you would in a git repository
4. To save the parameters resulting from fitting a [NEMS](https://www.bitbucket.org/lbhb/nems/) model
5. To save some predictions of the NEMS model on a data set from another researcher
6. To save some plots of the predictions of the NEMS model
7. To save a new data set compiled from multiple resaerchers' work
8. To publish or archive your code and data in a way that other researchers may easily reference and verify its accuracy and authenticity
9. To compare your newer model with your older, published models; or with other researchers' models


It's not that different from the many uses of git, except that jerbs encourage you to put data in the repository as well as code. Also like git, you may use jerbs even when not connected to the internet.


# Installation

Installing the jerb command line tool requires installing Python 3, cloning this git repo, and setting your `PATH` and `PYTHONPATH` environment variables. 

On Linux, just run this:

```
git clone http://bitbucket.com/lbhb/jerbs
JERB_GIT_REPO_PATH=`pwd`/jerbs
echo 'export PYTHONPATH="$PYTHONPATH:'$JERB_GIT_REPO_PATH'"
export PATH="$PATH:'$JERB_GIT_REPO_PATH'/jerb/bin"' >> ~/.bashrc
source ~/.bashrc
```

You should also set your username if you want to publish jerbs: (TODO: Require user registration)

```
git config --global jerb.user "anonymous"
```

If the installation worked, you should be able to view rudimentary help using:

```
jerb help
```

# Quick Tutorial

Let's just jump right in to see how jerbs might be useful to you. 

## Creating a jerb

```
   # Create a repo named anything
   jerb init temprepo
   cd temprepo
   cp /path/to/file1.txt /path/to/file2.txt ./
   git add file1.txt file2.txt
   git commit -m "This commit message will be completely ignored."
 
   # Edit the metadata:
   jerb meta   
   
   # Create the jerb
   jerb jerb >> myjerb.jerb

   # Look at the jerb, confirm that the metadata is as you want.

   # Then, publish the jerb for other people to use (including yourself)
   jerb publish myjerb.jerb
   #+END_EXAMPLE
```


## Creating a modified jerb

TODO

## Creating a data set

TODO

## Creating a NEMS model

TODO

## Publishing your code

TODO

## Fitting results

TODO


# Jerb Properties

From here on, this document is one big TODO

From here on, this document is one big TODO

From here on, this document is one big TODO

From here on, this document is one big TODO

## Indexable

By default jerbs are flat files, which are self-contained and easily shared. Unfortunately, flat files are hard to search through quickly. To accomplish this, it is common to index jerbs by any property found in the "metadata" field of a jerb. The jerb_index (i.e. "tracking") server is in fact designed to help you find specific Jerbs quickly, but it does not actually store them. 

## Shareable

   There are no restrictions on Jerbs that you use privately, however Jerbs committed to the public repo have the following restrictions:

   1. They must be <10kB in size. If you want to add large files, you are encouraged to put them somewhere web-accessible, and then download them inside your execution script.

   2. They must have <1kB of metadata. 

   These restrictions are because we don't have the money to store everybody's data yet, and the central git server is a potential bottleneck if not properly managed as a shared resource.

## Metadata
   The jerb "metadata" system lets each user attach their own single (editable, change-tracked) JSON to a jerb repo. Metadata provides a way to annotate the repo to describe what its dependencies are, to tag it with searchable keywords, describe what project it is for, and record other information about the jerb.  
   
   Metadata _must_ be formatted as a JSON. If the data is somehow /not/ a JSON, you will be unable to create any Jerbs using 'jerb jerb' and you will receive an error message asking you to fix the problem. (e.g. use 'git notes edit jerb_metadata' in such a case). The rationale for storing metadata as a JSON is because we want to always be able to serialize the metadata as a JSON, and if we stored it in another form this might not be possible.
   
   Aside: Internally, metadata uses the =git notes= system, so it has many of the same limitations that =git notes= has. Specifically, metadata is not pulled/fetched by default, so its history disappears with the original git repo.

   While the original repo exists, metadata is version controlled like everything else. If you want to look through the history of metadata, run "git log -p notes/metadata jerb_metadata".

## Branches and Refs

   Jerbs unfortunately don't quite have branches like git. Jerbs only include a single commit, created at the moment the jerb is created. Therefore, they cannot have a true git history, and the concept of 'branches' is not exactly the same. 

   We can, however, use the concept of a "ref", a reference to a specific commit. 



# Frequently Asked Questions

## Do I need to write my models in Python to use Jerbs?

It is intended that jerbs be usable by developers using any language, and that you may even wish to have have multiple languages participating in some large computation. Although the server's implementation is written in Python, it is also possible that it would be useful for people to write jerbs in the language of their choice. (Footnote: if you see a design decision that locks you into Python, please let me know.)


## Is it safe to `exec` somebody else's jerb?

No! Or perhaps I should say: only if you trust them! The same is true of any `git` repository -- you shouldn't execute the code contained in it unless you have read the code or trust the author. Your security is your responsibility.

Aside: while you may create cryptographically-signed jerbs, it does not provide security, merely verification of the author. 

## How do I find my jerbs?

   Add metadata. 

   By default, several fields are automatically populated by the =jerb= script:
   
   | user    | which is run using                             |
   | ref     | the git ref name (i.e. branch)                 |
   | parents | a list of parent commit hashes this pulls from |

   It is highly encouraged to also fill in details for:

   | tags        | a list of search tags to index this jerb indexed    |
   | description | a string describing to humans what this jerb is for |

   You may also add your own metadata as desired. It will all be indexed. You may wish to include:

    - Who prepared this jerb, when, on what PC, with what environment
    - Where to start execution of this jerb, if it is executable
    - Keywords or tags so that you may search for this jerb later
   

## Why shouldn't I just use `git`?

Jerbs /are/ git repos under the hood, so you could technically do almost everything you do with jerbs using plain old `git`. Jerbs however do create conventions that make it easier for researchers to collaborate:

1. Jerbs provide extensions to simplify mixing and matching commits from multiple repositories
2. Exchanging repos is simpler
3. Jerbs have less typing in general
4. You can manage a larger number of branches

In a quip, jerbs are to `git` repos what NoSQL is to SQL -- stupider but better at scaling to systems with larger numbers of contributors.


## What are jerb parents for?

Jerbs are not strictly dependent on other jerbs in any way. However, if you want to show that one jerb is "based on" another jerb, you may optionally reference them as "parents". This means that collections of jerbs will form an acyclic, directed graph. In the same way that each `git` commit is based on a small modification of the previous commit(s), each jerb is probably based on a small modification or combination of previous jerbs (its "parents"). A jerb may have any number of parents.


## How do I share my jerbs? Can everybody see my jerbs?

Yes, everybody at LBHB can see your jerbs (for now), but no in general. TODO: more nuanced description once user authentication is in place.

If you want to share jerbs with other researchers, you have two options:

1. Send them the jerb directly via file transfer methods
2. Upload your jerb to the centralized repo, and send them the JID (or URL) for the jerb. 

   Jerbs are private for each user by default; you would have to guess a hash to discover somebody else's data, which is very difficult. Also, attempts to randomly guess hashes are easily detected and throttled.

## Why do I get a different jerb every time I generate a new one from the same repo?
   Because the commit created when the jerb is generated contains a timestamp.

   Our mantra is "jerbs are immutable and unique." It could be very confusing if two jerbs had the same JIDs but different metadata, so we prevent that from occuring by baking in the timestamp.



## What happens if I publish a bad result?

It's not a big deal. You can publish "mistakes" as much as you want; they will simply become dead branches in the computational tree, without impacting the saving of correct computations. You can also delete your mistakes, as long as nobody else is using them yet.

## Why are jerbs immutable?

Jerbs are immutable, meaning that they cannot be modified or mutated in place, for several reasons:

  1. It prevents problems in which two jerbs have the same name or ID number but different versions of the same source code. By being immutable, you can be sure that you are getting the same version of the data as the original author.
  2. If a jerb cannot change, it may be safely cached anywhere, anytime, and automatically. Caches will never become "stale" and cause bugs.
  3. Immutablity allows for massively parallel operations because multiple processes may use a jerb simultaneously and not need to coordinate their operations.
  4. Immutablity reduces the number of operations that jerb indexing systems need to support.
  5. Trees of immutable objects naturally provide an audit trail of every computation, because newer computations inherently are based on older ones.

## Can I use jerbs with my computational cluster?

Sure. It is also intended that jerbs be used in a variety of execution contexts: locally, on your machine during software development; simultaneously across multiple machines or computers in a clustered environment; in a lambda function in the cloud; or even spread across the internet in very large shared computations. 


## What is the pack file?

Internally, the pack is the .pack file of a tiny git repo that holds some code or data. 
  
A git repo is an advantageous storage format because it is well-documented, and easily expands into a git repository:

1. 'git add' and 'git commit' are familiar commands to developers
2. git can group multiple files together in any directory structure
3. git already hashes files so they may are uniquely identifiable
4. git compresses files using zlib to reduce disk use
5. git has built-in tools to help resolve merge conflicts between jerbs

## I edited the metadata in the JSON and jerbs.org won't accept my jerb. Why?

The `metadata` field of the jerb is purely a cache so that people can identify the jerb without actually unpacking it. It should not be edited by hand, because the integrity of a jerb is checked by ensuring that the JSON metadata matches what is actually checked in to the jerbs packed git repository. 

If you received an error, it probably means you edited the jerb's JSON without actually editing the git repo. Try again by starting with `jerb meta` and then generating a new jerb with `jerb jerb`. 

## How do the gory internal details of jerb metadata work?

First and foremost, using the commands in this section is not recommended unless something has gone horribly wrong. 

Still reading? Ok, fine. For those of you trying to debug something that has gone horribly wrong, let's review how jerbs and git repos interact. Normally, a "git note" is used to store metadata on a git object, which is usually either a 'blob', a 'tree', or a 'commit'. Because a jerb is semantically similar to a commit and also to a repo, a design decision was to make the entire repo have the same metadata, rather than connecting our metadata to specific 'hidden' commits that will be discarded when a jerb is generated. 

For this reason, internally, the jerb script creates a stupid blob object named "jerb_metadata" with the contents "jerb_metadata" in it, and uses the git notes system to annotate that object with notes. 

   Why do we just use "git notes" at all instead of an alternative solution like a flat "metadata.json" file in the root directory? It is mostly to avoid problems with merging two repositories together with `jerb merge`.  Because git notes are not merged by default, by not using the standard commit strategy, we keep accidental cruft from accumulating in repo, and keep the details of metadata largely invisible from users. 

   `jerb meta` is under the hood identical to:

   #+BEGIN_EXAMPLE
   git notes edit jerb_metadata
   #+END_EXAMPLE

## What happens if I publish two jerbs with the same ref?
   The more recent one is what other people will see. This is 99% of the time what you want to happen. If it is not, don't panic! All of the published jerbs are still there. You will just have to search for it in another way.

   Note that ALL versions of metadata are stored; once history is written, it cannot be unwritten. This implies that queries may return multiple results for the same data, and that it is up to the client/viewer to present this information in a way palatable to the user. 

## If I change the metadata on the Jerb, wouldn't that affect other people's queries as well?

Yes, which is why that changing metadata requires making a new Jerb.

## What happens to my commit messages when I make a Jerb?

They are lost, except the very last one. Jerbs squash all the commits together into the last one, because a jerb can only contain a single commit. This is intentional: you still have the freedom to explore changes to your code locally and let git manage your code. When the code works and is in a good state, you may then squash your changes together into a single commit (a jerb) that may be committed to the central repository, shared with others, or archived.

## Isn't this a very wasteful way of storing data compared to git?

It can be, yes. If you update the metadata, this implies you must create a new Jerb. And, if you are storing all your data in the cloud as flat jerb files, some data will be replicated multiple times. If, however, you use a centralized server from which to generate your jerbs, all existing files can simply be referenced instead of copied by the central git server.

   Regardless, to this is that you should always try to keep your jerbs as small as possible. The smaller they are, the less needs to be duplicated, and the easier it will be for other people to reference and reuse your code.

## How do you detect spoofed metadata or a pack? 

First, they would have to also change the JID, or break the SHA hash. Both of these are plausible for a talented hacker. But, if you are worried about people spoofing your metadata and inserting their own code, ensure that you use HTTPS always and that you sign your jerbs cryptographically with GPG, and then configure your enviornment to only run jerbs that are signed cryptographically. 

## How do we know who ran something, when?
   Metadata is purely convention; you may create your own metadata properties about each Jerb as you see fit. However, in the interests of cooperation, I might suggest these properties might be useful for introspection:

   |------------------+-------------------------------------------|
   | exec_host        | The host that executed the data           |
   | exec_ip          | The IP of the host that executed the JERB |
   | exec_time_start  | Execution start time                      |
   | exec_time_finish | Execution                                 |
   | exec_user        | The user who executed the src payload     |
   |------------------+-------------------------------------------|

   But of course, there may be other properties that you come up with, like the AWS execution context, the type of EC2 instance running the Jerb, and so on.

## How does my code and data stay secret?
   If you are using the HTTPS connection, nobody will be able to eavesdrop the URLs that you are visiting, which means that they cannot learn your JIDs. Mining hashes is easily detected on the server side and can result in blocking of clients trying to guess JIDs at random.

## How do attach documentation to the Jerb?
   If your documentation is too big for "description", you might add new metadata fields:

   | docs    | Documentation for this JERB, what its intent was. |
   | authors | ...                                               |
   | license | ...                                               |
   | DOI     | ...                                               |
   | Version | ...                                               |

## How do I back up the Jerb Store?
   There are three JerbStores:
   1. LocalJerbStore, which stores files in a directory
   2. git clone therepo
   
## Is there a way to cryptographically sign commits?
   Yes, you may use GPG to cryptographically sign git commits as usual. 
   TODO: Test this.

## If I delete my Jerb, will other people lose access to it as well?
   At the moment, Yes. 

   In the future, No; if someone else is using it, then it should not be deleted until they no longer have references to it. (TODO)

## How do I get my code to use the latest version other people's code?
   Make a new jerb. 
  
## How do I use this to preprocess data files?
   TODO. Example.

## How do people discover jerbs? 
   TODO. Example

## How do you handle merge conflicts?
   TODO. Git does it. 
    
## What does the HTTP protocol for Jerbs look like?
   TODO
   |-------------+-----------------------------------------------------------|
   | PUT(jid)    | Upload and INDEX Jerb                                     |
   | HEAD(jid)   | Return a 'preview' jerb? Or how many bytes it is? TODO    |
   | GET(jid)    | Get the full Jerb                                         |
   | DELETE(jid) | Remove and UNINDEX Jerb (TODO: iff there are no children) |
   |-------------+-----------------------------------------------------------|

## How do I update the Jerb metadata if it is immutable?
   You cannot update a Jerb because they represent moments in time, and you can't change the past. If you want a different but similar Jerb, clone it, modify the metadata, and then save the new copy:

   #+BEGIN_EXAMPLE
   jerb ref ivar hello | jerb clone 
   cd hello
   jerb meta
   jerb jerb >> new_hello.jerb
   jerb publish new_hello.jerb
   #+END_EXAMPLE
